This file is for taking notes as you go.

Looked through the files to familiarize myself with the structure.
Started the client server to check if everything was set up correctly

Checked the tasks in README.md

1. There is a test for the backend that is failing, fix the code so it passes.

Ran the tests and saw an error message in the terminal "{\"OK\"true}\n doesnt contain "\"ok\":true. Capitalized "ok". Ran the tests again, they are all passing.

2. There is a test for the backend where the assumptions about what needs to be tested is incorrect. The test passes, but doesn't guarantee compatibility with how the frontend expects to communicate. You need to think about how the two interact and are compatible, and change the test code to match.

Noticed that health check is failing in front end, even though the status code is passing, so the test problem is probably in TestHealthCheck. Going to look throught all the test first trying to understand what they are checking for.
Googled everything(details in main_test.go)
Everything seems fine but TestHealthCheck.
Checking what front end is expecting by console.logging "res" in CheckBackend method in Chat.vue. It is expecting res.data.ok but we only have capital OK in data. Checking where it's being created. In handlers.go changed OK to lowercase ok. Frontend stopped receiving this part of data. Googled if struct types title must be capitalized. Learned that if it's not capitalized, it doesn't get exported. Changed it back.

Seems like TestHealthCheck is checking whether the data it gets from "http://localhost:3000/health" matches the health body? and if the response string contains "\"OK\":true", which it does. It never checks what data front end expects, and never catches the fact that front end was expecting lower-case "ok". Googled how to check what data front end expects golang testing. It doesnt look like the best thing to google. For now capitalized "ok" in res.data in Chat.vue. Health checks are passing now. Will come back to this if have time. Sometimes when I am stuck, switching to a different task and coming back helps.

3. In the frontend, make it so the user can enter a name that gets sent with every message and displayed to other users.

Found newMessage text field which sends currentMessage to the store(?) by calling sendMessage method on button click. So i need to add an extra text field where a user can add their name once and that will be added to every currentMessage.
Added "name" field to data to data. Now i need to pass it to sendMessage with every currentMessage. Added a second parameter(name) to sendMessage method. Now message variable in sendMessage method has an extra field with the name and value.
Where am I getting the message friom the server back? and when does the store come into play? Haven't worked with View but it looks very similar to Redux.
Usually actions update mutations which the change the state, but it is not doing that.
Looked for where SOCKET_ONMESSAGE is being called by searching files, didn't find it, but remember seeing onmessage in Chat.vue. Looks like the place I am getting a message back from the server and adds the data to the messages array, then <v-chip> component gets rendered witth message.text, added message.name and it worked. :)
Was gonna write comments in Chat.vue as well but most definitely out of time. Got carried awat a bit. :)
