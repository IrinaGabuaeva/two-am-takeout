package handlers

import (
	"encoding/json"
	"net/http"

	"gitlab.com/upchieve/two-am-takeout/server/sockets"
)

// HealthBody is the response body for a health check
type HealthBody struct {
	//changed OK to lowercase ok. Frontend stopped receiving this part of data. Googled if struct types title must be capitalized. Learned that if it's not capitalized, it doesn't get exported. Changed it back.
	OK bool `json:"OK"`
}

// ServeHealth handles healthcheck requests
//gets called when server starts & sets HealthBody OK to true
func ServeHealth(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "http://localhost:8080")
	json.NewEncoder(w).Encode(HealthBody{OK: true})
}

// ServeSocket returns a socket handler
func ServeSocket(h *sockets.Hub) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		sockets.ServeWs(h, w, r)
	}
}
