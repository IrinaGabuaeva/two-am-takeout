package main

import (
	"context"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/url"
	"testing"
	"time"

	"github.com/gorilla/websocket"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/suite"

	"gitlab.com/upchieve/two-am-takeout/server/handlers"
)

var ctx context.Context
var cancel context.CancelFunc

type ServerSuite struct {
	suite.Suite
	ctx    context.Context
	cancel context.CancelFunc
}
// this runs before the tests in the suite are run and calls startServer. Searched the files for startServer. Read through it. I guess it starts the server? :)
func (suite *ServerSuite) SetupSuite() {
	ctx = context.Background()
	suite.ctx, suite.cancel = context.WithCancel(ctx)

	go startServer(ctx)

	time.Sleep(1000)
}
// this is also self-explanatory
func (suite *ServerSuite) TeardownSuite() {
	suite.cancel()
}

//I think this is where the problem is. This function checks if health body 
func (suite *ServerSuite) TestHealthCheck() {
	resp, err := http.Get("http://localhost:3000/health")
	assert.Nil(suite.T(), err)
	//this makes sure that response's status code matches status code 200(which it does)
	assert.Equal(suite.T(), http.StatusOK, resp.StatusCode)

	respData, err := ioutil.ReadAll(resp.Body)
	assert.Nil(suite.T(), err)

	var respBody handlers.HealthBody
	err = json.Unmarshal(respData, &respBody)
	assert.Nil(suite.T(), err)

	// make sure health body works matches what the frontend is expecting
	//checking if the response string contains "OK:true", which it does but in never checks what data front end is expecting.
	respString := string(respData)
	assert.Contains(suite.T(), respString, "\"OK\":true")
	//capitalized "ok" to match HealthBody
}


//This creates reader and sender client connections, writes a message and compares if it equals in the reader.ReadMessage function. Seems fine.
func (suite *ServerSuite) TestWebsockets() {
	u := url.URL{Scheme: "ws", Host: "localhost:3000", Path: "/ws"}

	reader, _, err := websocket.DefaultDialer.Dial(u.String(), nil)
	defer reader.Close()
	assert.Nil(suite.T(), err)

	sender, _, err := websocket.DefaultDialer.Dial(u.String(), nil)
	defer sender.Close()
	assert.Nil(suite.T(), err)

	// checking that text formatted messages go through and can be read correctly
	sender.WriteMessage(websocket.TextMessage, []byte("Hello, Websocket!"))

	_, message, err := reader.ReadMessage()
	assert.Nil(suite.T(), err)
	assert.Equal(suite.T(), "Hello, Websocket!", string(message))
}

func TestServerSuite(t *testing.T) {
	suite.Run(t, new(ServerSuite))
}
